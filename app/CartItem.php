<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public function item(){
        return $this->belongsTo('App\Item');
    }

    public function uom(){
        return $this->belongsTo('App\Uom');
    }

}
