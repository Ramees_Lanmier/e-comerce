<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{

    public function passwordRequestOtp()
    {
        return view('auth.otpverify');
    }

    public function sendOtp(Request $request)
    {
        // dd(rand(100000, 999999));
        $otp = rand(100000, 999999);
        $data = ["otp" => $otp];

        $user = User::where('email', $request->email)->first();
        if ($user) {
            $user->otp = $otp;
            $user->save();
            Mail::send(['text' => 'otp_mail'], $data, function ($message) use ($request) {
                $message->to($request->email, '  ')->subject('OTP Mail- Fashion Hub');
                $message->from('fasionhub@ggmail.com', 'FasionHub');
            });
            return "OTP Code : " . $otp;
        } else {
            return "This Email ID is not registered on fasion Hub, Please sign Up first";
        }
    }

    public function changePasswordView(Request $request)
    {
        $data = [
            'email' => $request->email,
            'otp' => $request->otp
        ];
        return view('auth.change_password', $data);
    }

    public function changePassword(Request $request)
    {
        $email = $request->email;
        $otp = $request->otp;
        $password = $request->password;

        $user = User::where('email', $email)->where('otp', $otp)->first();
        if ($user) {
            $user->password = bcrypt($password);
            $user->otp = null;
            $user->save();
            return "Password Changed Successfully";
        }else{
            return "UNAUTHERIZED";
        }
    }
}
