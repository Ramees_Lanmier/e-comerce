<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class FrondendController extends Controller
{
    public function index()
    {
        // return view('home');
        $items = Item::get();
        $data = ['items' => $items];
        // dd($items[0]->uoms);
        return view('pages.index',$data);
    }

    public function productView(Request $request){
        $item_id = $request->item_id;
        $uom_id = $request->uom_id;
        $item = Item::where('id',$item_id)->first();
        $data = ['item' => $item,'uom_id' => $uom_id];
        // dd($items[0]->uoms);
        return view('pages.product_view',$data);
    }

}
