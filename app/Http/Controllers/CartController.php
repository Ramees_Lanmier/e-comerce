<?php

namespace App\Http\Controllers;

use App\CartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Ui\Presets\React;

class CartController extends Controller
{
    public function index(){
        $total = 0;
        $user_id = $userId = Auth::id();
        $cartItems = CartItem::where('user_id',$user_id)->get();
        foreach ($cartItems as $key => $cart) {
            $total += $cart->quantity * $cart->uom->price;
        }
        $data = ['cartItems' => $cartItems,'total'=>$total];
        // dd($cartItems[0]->uom);
        return view('pages.cart',$data);
    }

    public function add(Request $request){
        // dd($request->item_id);
        $user_id = $userId = Auth::id();;
        $cartItems = CartItem::where('item_id',$request->item_id)->where('uom_id',$request->uom_id)->where('user_id',$user_id)->first();
        if($cartItems){
            $cartItems->item_id = $request->item_id;
            $cartItems->uom_id = $request->uom_id;
            $cartItems->quantity = $cartItems->quantity + $request->quantity;
            $cartItems->save();
        }else{
            $cartItems = new CartItem();
            $cartItems->item_id = $request->item_id;
            $cartItems->uom_id = $request->uom_id;
            $cartItems->quantity = $request->quantity;
            $cartItems->user_id = $user_id;
            $cartItems->save();
        }
    }
}
