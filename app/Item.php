<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function uoms(){
        return $this->hasMany('App\Uom');
    }
}
