<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/password_request_otp', 'AuthController@passwordRequestOtp')->name('password.request-otp');
Route::get('/send_otpp', 'AuthController@sendOtp')->name('password.send_otp_view');


Route::post('/send_otp', 'AuthController@sendOtp')->name('password.send_otp');
Route::post('/change_password_view', 'AuthController@changePasswordView')->name('password.change_password_view');
Route::post('/change_password', 'AuthController@changePassword')->name('password.change_password');

Auth::routes();

Route::get('/', 'FrondendController@index')->name('home');
Route::get('/product_view', 'FrondendController@productView')->name('product_view');

Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/cart', 'CartController@add')->name('add_to_cart');




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
