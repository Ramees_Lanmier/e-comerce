@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">OTP Verification</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('password.change_password_view') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="otp" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input type="email" id="email" class="form-control" name="email" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="button" class="btn btn-primary" id="sendOtpBtn">
                                    Send OTP
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label for="otp" class="col-md-4 col-form-label text-md-right">OTP</label>

                            <div class="col-md-6">
                                <input id="otp" type="number" class="form-control" name="otp" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>

                        <code id="otpDisplay"></code>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script>

        $("#sendOtpBtn").click(function(){
            var email = $('#email').val();
            console.log(email);
            $("#sendOtpBtn").prop("disabled",true);
            $("#sendOtpBtn").html("Sending...");
            $.ajax({
            type: "POST",
            url: "{{ route('password.send_otp') }}",
            data: {
                "email" : email,
                "_token": "{{ csrf_token() }}",
            },
            cache: false,
            success: function(data){
                $("#sendOtpBtn").prop("disabled",false);
                $("#sendOtpBtn").html("Resend OTP");
                $("#otpDisplay").html( data);
            }
            });
         });
    </script>
@endsection
