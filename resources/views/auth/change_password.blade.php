@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Change Password</div>

                <div class="card-body">

                    {{-- <form method="POST" action="{{ route('login') }}">
                        @csrf --}}

                        <div class="form-group row">
                            <label for="otp" class="col-md-4 col-form-label text-md-right">New Password </label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" autofocus>
                            </div>
                        </div>

                        <input type="hidden" name="email">
                        <input type="hidden" name="otp">

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button id="submitBtn" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    {{-- </form> --}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script>

        $("#submitBtn").click(function(){
            // alert()
            var password = $('#password').val();
            $("#submitBtn").prop("disabled",true);
            $("#submitBtn").html("Submiting...");
            $.ajax({
            type: "POST",
            url: "{{ route('password.change_password') }}",
            data: {
                "email" : "{{ $email }}",
                "otp" : "{{ $otp }}",
                "password" : password,
                "_token": "{{ csrf_token() }}",
            },
            cache: false,
            success: function(data){
                // $("#otpDisplay").html("OTP Code : " + data);
                alert(data);
                // window.location = "/login"
            }
            });
         });
    </script>
@endsection
