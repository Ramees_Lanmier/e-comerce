@extends('layouts.master')

@section('content')
    <!-- inner banner -->
    <div class="ibanner_w3 pt-sm-5 pt-3">
        <h4 class="head_agileinfo text-center text-capitalize text-center pt-5">
            <span>f</span>ashion
            <span>h</span>ub</h4>
    </div>
    <!-- //inner banner -->
    <!-- breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            {{-- <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Contact Us</li> --}}
        </ol>
    </nav>
    <!-- //breadcrumbs -->
    <!-- Single -->
    <div class="innerf-pages section py-5">
        <div class="container">
            @foreach ($item->uoms as $uom)
                @if ($uom->id == $uom_id)
                    <div class="row my-sm-5">
                <div class="col-lg-4 single-right-left">
                    <div class="grid images_3_of_2">
                        <div class="flexslider1">
                            <ul class="slides">
                                <li data-thumb="images/ms1.jpg">
                                    <div class="thumb-image">
                                    <img src="{{ asset('storage/'.$uom->image_url) }}" data-imagezoom="true" alt=" " class="img-fluid"> </div>
                                </li>
                                {{-- <li data-thumb="images/ms2.jpg">
                                    <div class="thumb-image">
                                        <img src="images/ms2.jpg" data-imagezoom="true" alt=" " class="img-fluid"> </div>
                                </li> --}}
                                {{-- <li data-thumb="images/ms3.jpg">
                                    <div class="thumb-image">
                                        <img src="images/ms3.jpg" data-imagezoom="true" alt=" " class="img-fluid"> </div>
                                </li> --}}
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 mt-lg-0 mt-5 single-right-left simpleCart_shelfItem">
                <h3>{{ $item->name }} ({{ $uom->name}})</h3>
                    </h3>
                    <div class="caption">

                        <ul class="rating-single">
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"> </div>
                        <h6>
                            ${{ $uom->price }}</h6>
                    </div>
                    <div class="desc_single">
                        <h5>Description</h5>
                        <p>{{ $item->description }}</p>
                    </div>
                    {{-- <div class="d-sm-flex justify-content-between">
                        <div class="occasional">
                            <h5 class="sp_title mb-3">Highlights</h5>
                            <ul class="single_specific">
                                <li>
                                    <span>Fabric: </span> Poly-Viscose</li>
                                <li>
                                    <span>Pattern :</span> Solid</li>
                                <li>
                                    <span>Type :</span> Single Breasted</li>
                                <li>
                                    <span>Fit :</span> Slim</li>
                            </ul>

                        </div>
                        <div class="color-quality mt-sm-0 mt-4">
                            <h5 class="sp_title mb-3">services</h5>
                            <ul class="single_serv">
                                <li>
                                    <a href="#">30 Day Return Policy</a>
                                </li>
                                <li class="mt-2">
                                    <a href="#">Cash on Delivery available</a>
                                </li>
                            </ul>
                        </div>
                    </div> --}}
                    {{-- <div class="description">
                        <h5>Check delivery, payment options and charges at your location</h5>
                        <form action="#" method="post">
                            <input type="text" placeholder="Enter pincode" required />
                            <input type="submit" value="Check">
                        </form>
                    </div> --}}
                    <div class="mb-2">
                        <strong><span style="font-size: larger;">{{$item->uom_title}} : </span></strong>
                        @foreach ($item->uoms as $uom)
                    <a  href="{{ 'product_view?item_id='.$uom->item_id.'&uom_id='.$uom->id}}" class="btn btn-outline-dark ml-2">{{ $uom->name }}</a>
                        @endforeach
                    </div>
                    @auth
                        <div class="occasion-cart">
                            <div class="chr single-item single_page_b">
                                <button type="button" id="cartBtn" onclick="addToCart({{ $uom->item_id}},{{ $uom_id }})" class="hub-cart phub-cart btn" style="color: #0076be;">
                                    <i class="fa fa-cart-plus" aria-hidden="true"></i> Add To Cart 
                                </button>
                            </div>
                        </div>
                    @endauth 
                </div>
            </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection

@section('script')

<script>

    function addToCart(itemId,uomId,quantity=1){
        console.log("itemId",itemId);
        console.log("uomId",uomId);
        
        $("#cartBtn").prop("disabled",true);
        $("#cartBtn").html("Adding To Cart...");
        $.ajax({
        type: "POST",
        url: "{{ route('add_to_cart') }}",
        data: {
            'item_id' : itemId,
            'uom_id' : uomId,
            'quantity' : quantity
        },
        cache: false,
        success: function(data){
            alert('Item successfully addet to cart')
            $("#resultarea").text(data);
            window.location = "/";
        }
        });
    }

</script>
    
@endsection